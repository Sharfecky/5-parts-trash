package ru.otus.junit.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.otus.junit.domain.Person;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DisplayName("Класс PersonDaoImpl")
class PersonDaoImplTest {
    private final PersonDaoImpl personDaoImpl = new PersonDaoImpl();

    @BeforeEach
    void setUp() {
        personDaoImpl.getAll().clear();

        personDaoImpl.getAll().add(new Person(1, "One"));
        personDaoImpl.getAll().add(new Person(2, "Two"));
        personDaoImpl.getAll().add(new Person(3, "Three"));
    }

    @DisplayName("должен получить персону по имени")
    @Test
    void getByNameTest() {
        final var testPerson = personDaoImpl.getByName("One");

        assertThat(testPerson)
                .isNotNull()
                .matches(person -> person.getName().equals("One") && person.getAge() == 1);
    }

    @DisplayName("должен получить исключение при запросе персоны по имени")
    @Test
    void getByNameTestWithException() {
        assertThatThrownBy(() -> personDaoImpl.getByName("Four"))
                .isInstanceOf(PersonNotFoundException.class);
    }

    @DisplayName("должен получить всех персон")
    @Test
    void getAllTest() {
        final var all = personDaoImpl.getAll();

        assertThat(all)
                .hasSize(3)
                .anyMatch(person -> person.getName().equals("One") && person.getAge() == 1)
                .anyMatch(person -> person.getName().equals("Two") && person.getAge() == 2)
                .anyMatch(person -> person.getName().equals("Three") && person.getAge() == 3);
    }

    @DisplayName("должен получить персону по имени")
    @Test
    void deleteByNameTest() {
        personDaoImpl.deleteByName("One");

        final var all = personDaoImpl.getAll();

        assertThat(all)
                .hasSize(2)
                .anyMatch(person -> person.getName().equals("Two") && person.getAge() == 2)
                .anyMatch(person -> person.getName().equals("Three") && person.getAge() == 3);
    }

    @DisplayName("должен получить исключение при запросе удаления персоны по имени")
    @Test
    void deleteByNameTestWithException() {
        assertThatThrownBy(() ->  personDaoImpl.deleteByName("Noise"))
                .isInstanceOf(PersonNotFoundException.class);
    }

    @DisplayName("должен сохранить person")
    @Test
    void saveTestSave() {
        personDaoImpl.save(new Person(4,"Four"));

        final var all = personDaoImpl.getAll();

        assertThat(all)
                .hasSize(4)
                .anyMatch(person -> person.getName().equals("One") && person.getAge() == 1)
                .anyMatch(person -> person.getName().equals("Two") && person.getAge() == 2)
                .anyMatch(person -> person.getName().equals("Three") && person.getAge() == 3)
                .anyMatch(person -> person.getName().equals("Four") && person.getAge() == 4);
    }

    @DisplayName("должен заменить person")
    @Test
    void saveTestReplace() {
        personDaoImpl.save(new Person(33,"Three"));

        final var all = personDaoImpl.getAll();

        assertThat(all)
                .hasSize(3)
                .anyMatch(person -> person.getName().equals("One") && person.getAge() == 1)
                .anyMatch(person -> person.getName().equals("Two") && person.getAge() == 2)
                .anyMatch(person -> person.getName().equals("Three") && person.getAge() == 33);
        
    }

}