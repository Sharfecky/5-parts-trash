package ru.otus.junit.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.otus.junit.dao.PersonDao;
import ru.otus.junit.domain.Person;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class PersonServiceImplTest {
    @Mock
    private PersonDao personDao;
    @InjectMocks
    private PersonServiceImpl personService;

    @Test
    void getByName() {
        given(personDao.getByName(eq("Ivan"))).willReturn(new Person(11, "Ivan"));

        assertThat((personService.getByName("Ivan")).getName()).isEqualTo("Ivan");
        assertThat((personService.getByName("Ivan")).getAge()).isEqualTo(11);
    }

    @Test
    void existsWithNameTest() {
        given(personDao.getByName("Two")).willReturn(new Person(10, "SuperBoya2"));
        assertThat(personDao.getByName("Two").getName()).isEqualTo("SuperBoya2");
        assertThat(personService.existsWithName("Two")).isTrue();

    }

    @Test
    void getAllTest() {
        ArrayList<Person> ans = new ArrayList<Person>();
        ans.add(new Person(1, "One"));
        ans.add(new Person(2, "Two"));

        given(personDao.getAll()).willReturn(ans);
        assertThat(personDao.getAll().size()).isEqualTo(2);

    }

    @Test
    void saveTest() {
        ArrayList<Person> ans = new ArrayList<Person>();
        ans.add(new Person(1, "One"));
        ans.add(new Person(2, "Two"));

        given(personDao.getAll()).willReturn(ans);
        assertThat(personDao.getAll().size()).isEqualTo(2);
    }
}
