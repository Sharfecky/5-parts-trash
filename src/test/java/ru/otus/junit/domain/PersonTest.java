package ru.otus.junit.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Класс Person")
class PersonTest {

    @DisplayName("корректно создаётся конструктором")
    @Test
    void shouldHaveCorrectConstructor() {
        var person = new Person(10, "Fucking Slave!");

        assertEquals("Fucking Slave!", person.getName());
        assertEquals(10, person.getAge());
    }

    @DisplayName("корректно присваиваивается имя")
    @Test
    void setNameTest() {
        Person person = new Person(42, "Ivan");
        person.setName("Van Darkholme");
        assertEquals("Van Darkholme", person.getName());
    }

    @DisplayName("корректно присваиваивает возраст")
    @Test
    void setAgeTest() {
        Person person = new Person(47, "Dungeon Master");
        person.setAge(1337);
        assertEquals(1337, person.getAge());
    }

    @DisplayName("корректно инкрементирует возраст")
    @Test
    void birthDayTest() {
        Person person = new Person(47, "Dungeon Master");
        person.birthDay();
        assertEquals(48, person.getAge());
    }

    @DisplayName("Конструктор AssertJ Version")
    @Test
    void ConstructorAssertJTest() {
        Person person = new Person(10, "Fucking Slave!");
        assertThat(person.getName()).isEqualTo("Fucking Slave!");
    }


    @DisplayName("РРРеНэйминг AssertJ Version")
    @Test
    void setNameAssertJTest() {
        Person person = new Person(42, "Ivan");
        person.setName("Van Darkholme");
        assertThat(person.getName()).isEqualTo("Van Darkholme");
    }

    @DisplayName("REEEEзаписать Возраст AssertJ Version")
    @Test
    void setAgeAssertJTest() {
        Person person = new Person(47, "Dungeon Master");
        person.setAge(1337);
        assertThat(person.getAge()).isEqualTo(1337);
    }

    @DisplayName("корректно инкрементирует возраст AssertJ Version")
    @Test
    void birthDayAssertJTest() {
        Person person = new Person(47, "Dungeon Master");
        person.birthDay();
        assertEquals(48, person.getAge());
        assertThat(person.getAge()).isEqualTo(48);
    }


    // TODO: @DisplayName("должен")

    // TODO: @DisplayName("должен увеличивать возраст при вызове birthDay")
}
