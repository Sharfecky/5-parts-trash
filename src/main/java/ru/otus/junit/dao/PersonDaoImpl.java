package ru.otus.junit.dao;

import ru.otus.junit.domain.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonDaoImpl implements PersonDao {
    private final List<Person> people = new ArrayList<>();

    @Override
    public Person getByName(String name) throws PersonNotFoundException {
        for (Person person1 : people) {
            if (person1.getName().equals(name)) {
                return person1;
            }
        }

        throw new PersonNotFoundException("FUCK UUUUUU");
    }

    @Override
    public List<Person> getAll() {
        return people;
    }

    @Override
    public void deleteByName(String name) throws PersonNotFoundException {
        try {
            final var person = getByName(name);

            people.remove(person);
        } catch (PersonNotFoundException ex) {
            System.err.println("При удалении ошибка! Юзер не найден!");

            throw ex;
        }
    }

    @Override
    public void save(Person person) {
        boolean isUpdated = false;

        for (int i = 0; i < people.size(); i++) {
            final var person1 = people.get(i);
            if (person1.getName().equals(person.getName())) {
                people.set(i,person);
                isUpdated = true;
            }
        }

        if (!isUpdated) {
            people.add(person);
        }
    }
}